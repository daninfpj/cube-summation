// Output for the web
const os = require("os");

function WebPresenter(output) {
	this.output = output;
}

WebPresenter.prototype.render = function() {
	return this.output.join(os.EOL);
};

module.exports = WebPresenter;
