# BACK-END DEVELOPER TECHNICAL SKILLS

## CODING CHALLENGE

La implementación de fuerza bruta del algoritmo es demasiado lenta `(O(n^3))` y no pasa en HackerRank. Esta implementación utiliza *memoization* y es suficiente para pasar en HackerRank, por lo tanto la óptima usando un árbol binario indexado sería *overkill*.

La aplicación fue desarrollada en Node.js siguiendo una metología TDD: primero se escriben las pruebas y estas van indicando paso a paso el código que se debe escribir hasta que pasen. Luego se refactoriza. La cobertura de las pruebas es del 100%.

### Demo online

https://daninfpj-cube-summation.herokuapp.com/

### Ejecutar localmente
- Clonar

    ```
    git clone git@bitbucket.org:daninfpj/cube-summation.git
    ```

- Instalar dependencias

    ```
    yarn
    ```

- Ejecutar la aplicación

    ```
    npm start
    ```

- Ejecutar las pruebas

    ```
    npm test
    ```

### Capas de la aplicación

1. Modelos (`/models`)

    Las capa de los modelos incluye las clases que modelan la lógica de la aplicación.

    - `Block`

        Modela un punto de la matriz, con sus coordenadas y peso.

    - `Cube`

        Modela la matriz y las operaciones que se realizan sobre ella: `update` y `query`.

    - `Challenge`

        Modela el problema. Por cado caso instancia un `Cube`, realiza sobre este las operaciones y guarda la salida de las operaciones relevantes (`query`).


2. Presentación (`/presenters`, `/views`)

    Estas clases se encargan de la presentación del resultado del problema. También se incluye en esta capa la vista `html` para el template engine `Pug`.

    - `WebPresenter`

        Este controlador formatea la salida del problema para web.

    Organizado de esta forma, el problema se podría adaptar para otros medios (por ejemplo salida por consola) agregando un nuevo `Presenter`, sin necesidad de modificar los modelos.

3. Helpers (`/support`)

    Clases que ayudan con responsabilidades que no pertenecen a ninguna de las otras capas.

    - `InputParser`

        Esta clase transforma la entrada en una estructura apropiada para la clase `Challenge`.

    De manera similar a los `Presenters`, esta organización permite agregar nuevos `Parsers` para entradas diferentes, sin modificar la capa de los modelos.

4. Controladores (`/controllers`)

    Estas clases reciben la petición HTTP, instancian los modelos apropiados y les proveen los parámetros necesarios. Finalmente envían la respuesta al navegador del usuario.

    - `ChallengeController`

        Este controlador recibe el input del problema, lo formatea con ayuda de `InputParser` para pasarlo a una nueva instancia de `Challenge`. Finalmente utiliza `WebPresenter` para enviar la respuesta al navegador.
