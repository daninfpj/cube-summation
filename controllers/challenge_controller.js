const sample_input = require("../test/support/sample_input");
const InputParser = require("../support/input_parser");
const Challenge = require("../models/challenge");
const WebPresenter = require("../presenters/web_presenter");

function process(input, res) {
	const parsedInput = new InputParser(input);
	const output = new WebPresenter(new Challenge(parsedInput).run()).render();

	res.render("index", { input: input, output: output });
}

function ChallengeController() {}

ChallengeController.prototype.get = function(req, res) {
	process(sample_input, res);
};

ChallengeController.prototype.post = function(req, res) {
	process(req.body.input, res);
};

module.exports = ChallengeController;
