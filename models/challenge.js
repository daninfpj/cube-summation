// Entry point to solve challenge
const Cube = require("./cube");

function Challenge(parsedInput) {
	this.cases = parsedInput.cases;
	this.output = [];
}

Challenge.prototype.run = function() {
	this.cases.forEach(operations => {
		let cube = new Cube();

		operations.forEach(operation => {
			switch (operation.operation) {
				case "update":
					cube.update.apply(cube, operation.args);
					break;
				case "query":
					this.output.push(cube.query.apply(cube, operation.args));
					break;
			}
		});
	});

	return this.output;
};

module.exports = Challenge;
