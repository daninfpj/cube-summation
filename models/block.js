// Represents a single block of the matrix
function Block(x, y, z, w) {
	this.x = x - 1;
	this.y = y - 1;
	this.z = z - 1;
	this.w = w || 0;
}

Block.prototype.inRange = function(fromBlock, toBlock) {
	return (
		this.x >= fromBlock.x &&
		this.x <= toBlock.x &&
		this.y >= fromBlock.y &&
		this.y <= toBlock.y &&
		this.z >= fromBlock.z &&
		this.z <= toBlock.z
	);
};

Block.prototype.coords = function() {
  return this.x + "," + this.y + "," + this.z;
}

module.exports = Block;
