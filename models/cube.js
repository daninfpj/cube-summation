// Represents the matrix
const Block = require("./block");

function Cube() {
	this.updated = new Map();
}

Cube.prototype.update = function(x, y, z, w) {
	let block = new Block(x, y, z, w);

	this.updated.set(block.coords(), block);

	return this;
};

Cube.prototype.updatedInRange = function(fromBlock, toBlock) {
	return Array.from(this.updated.values()).filter(block =>
		block.inRange(fromBlock, toBlock)
	);
};

Cube.prototype.query = function(x1, y1, z1, x2, y2, z2) {
	let fromBlock = new Block(x1, y1, z1);
	let toBlock = new Block(x2, y2, z2);

	return Array.from(this.updatedInRange(fromBlock, toBlock)).reduce(
		(sum, block) => (sum += block.w),
		0
	);
};

module.exports = Cube;
