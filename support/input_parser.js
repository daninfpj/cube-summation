// Parse input
function InputParser(input) {
	this.cases = [];

	let i_lines = input.split("\n");

	let n_cases = parseInt(i_lines.shift());

	while (n_cases > 0) {
		let n_m = i_lines.shift().split(" ");
		let n = n_m[0];
		let m = n_m[1];
		let operations = [];

		while (m > 0) {
			let cmd = i_lines.shift().split(" ");

			switch (cmd[0]) {
				case "UPDATE":
					let x = parseInt(cmd[1]);
					let y = parseInt(cmd[2]);
					let z = parseInt(cmd[3]);
					let w = parseInt(cmd[4]);

					operations.push({ operation: "update", args: [x, y, z, w] });

					break;
				case "QUERY":
					let x1 = parseInt(cmd[1]);
					let y1 = parseInt(cmd[2]);
					let z1 = parseInt(cmd[3]);
					let x2 = parseInt(cmd[4]);
					let y2 = parseInt(cmd[5]);
					let z2 = parseInt(cmd[6]);

					operations.push({
						operation: "query",
						args: [x1, y1, z1, x2, y2, z2]
					});

					break;
			}

			m--;
		}

    this.cases.push(operations);

		n_cases--;
	}
}

module.exports = InputParser;
