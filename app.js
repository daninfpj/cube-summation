const express = require("express");
const bodyParser = require("body-parser");
const ChallengeController = require("./controllers/challenge_controller");

const app = express();
const controller = new ChallengeController();

app.use(express.static("public"));
app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true
	})
);

app.set("view engine", "pug");

app.get("/", controller.get);
app.post("/", controller.post);

app.listen(process.env.PORT || 3000);
