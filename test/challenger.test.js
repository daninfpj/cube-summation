const Challenge = require("../models/Challenge");
const InputParser = require("../support/input_parser");
const input = require("./support/sample_input");

test("Solve challenge", () => {
  let parsedInput = new InputParser(input);
  expect(new Challenge(parsedInput).run()).toEqual([4, 4, 27]);
});
