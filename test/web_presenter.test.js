const WebPresenter = require("../presenters/web_presenter");
const output = require("./support/sample_output");

test("WebPresenter", () => {
	expect(new WebPresenter([4, 4, 27]).render()).toEqual(output);
});
