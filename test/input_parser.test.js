const InputParser = require("../support/input_parser");
const input = require("./support/sample_input");

test("Input parser", () => {
	expect(new InputParser(input).cases).toEqual([
    [
      { operation: "update", args: [2, 2, 2, 4] },
      { operation: "query", args: [1, 1, 1, 3, 3, 3] },
      { operation: "update", args: [1, 1, 1, 23] },
      { operation: "query", args: [2, 2, 2, 4, 4, 4] },
      { operation: "query", args: [1, 1, 1, 3, 3, 3] },
    ]
  ]);
});
