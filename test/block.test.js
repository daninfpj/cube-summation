const Block = require("../models/block");

test("Block is created with coordinates", () => {
	let block = new Block(1, 2, 3);

	expect(block.x).toBe(0);
	expect(block.y).toBe(1);
	expect(block.z).toBe(2);
});

test("Block is within range", () => {
	let fromBlock = new Block(1, 1, 1);
	let toBlock = new Block(4, 4, 4);

	expect(new Block(2, 2, 2).inRange(fromBlock, toBlock)).toBeTruthy();
	expect(new Block(4, 4, 4).inRange(fromBlock, toBlock)).toBeTruthy();
	expect(new Block(5, 5, 5).inRange(fromBlock, toBlock)).toBeFalsy();
});

test("Block is created with weigth", () => {
	let block = new Block(1, 1, 1, 5);

	expect(block.w).toBe(5);
});

test("Block coords", () => {
	let block = new Block(1, 1, 1);

	expect(block.coords()).toBe("0,0,0");
});
