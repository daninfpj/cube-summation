const Cube = require("../models/cube");
const Block = require("../models/block");

test("Cube is created", () => {
	expect(new Cube().updated).toEqual(new Map());
});

test("Cube is updated", () => {
	let cube = new Cube();

	expect(cube.update(1, 1, 1, 2).updated).toEqual(
		new Map([["0,0,0", new Block(1, 1, 1, 2)]])
	);
	expect(cube.update(1, 1, 1, 4).updated).toEqual(
		new Map([["0,0,0", new Block(1, 1, 1, 4)]])
	);
});

test("Query cube", () => {
	let cube = new Cube();

	cube.update(2, 2, 2, 4);
	expect(cube.query(1, 1, 1, 3, 3, 3)).toEqual(4);

	cube.update(1, 1, 1, 23);
	expect(cube.query(2, 2, 2, 4, 4, 4)).toEqual(4);
	expect(cube.query(1, 1, 1, 3, 3, 3)).toEqual(27);
});
