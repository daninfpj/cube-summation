const os = require("os");

const input =
	"1" +
	os.EOL +
	"4 5" +
	os.EOL +
	"UPDATE 2 2 2 4" +
	os.EOL +
	"QUERY 1 1 1 3 3 3" +
	os.EOL +
	"UPDATE 1 1 1 23" +
	os.EOL +
	"QUERY 2 2 2 4 4 4" +
	os.EOL +
	"QUERY 1 1 1 3 3 3";

module.exports = input;
